# spec/robot_spec.rb
require_relative '../lib/robot.rb'

RSpec.describe Robot do

  let(:robot) { described_class.new(1, 1, :north, 5, 5) }
  
  context 'initialize' do
    let(:x) { 1 }
    let(:y) { 1 }
    let(:direction) { :left }
    let(:max_x) { 5 }
    let(:max_y) { 5 }
    
    it 'creates a robot when valid parameters' do
      expect { robot }.not_to raise_error
    end

    it 'raises error for unvalid max_x' do
      expect { described_class.new(x, y, direction, 0, max_y) }
        .to raise_error.with_message('Please place the Robot in a valid position')
    end

    it 'raises error for unvalid max_y' do
      expect { described_class.new(x, y, direction, max_x, 0) }
        .to raise_error.with_message('Please place the Robot in a valid position')
    end

    it 'raises error for unvalid x' do
      expect { described_class.new(-1, y, direction, max_x, max_y) }
        .to raise_error.with_message('Please place the Robot in a valid position')
    end

    it 'raises error for unvalid x' do
      expect { described_class.new(x, -1, direction, max_x, max_y) }
        .to raise_error.with_message('Please place the Robot in a valid position')
    end
  end

  context 'directions' do
    it 'turns to the left' do
      robot.left
      expect(robot.direction).to eq(:west)
    end

    it 'turns to the right' do
      robot.right
      expect(robot.direction).to eq(:east)
    end
  end

  context 'report' do
    it 'comunicates current position and direction' do
      expect(robot.report).to eq('Report: Hi, I am placed at [1,1], facing north')
    end
  end

  context 'move' do
    context 'according to direction' do
      it 'to west' do
        robot.direction = :west              
        expect{ robot.move }.to change{ robot.x }.from(1).to(0)
      end

      it 'to east' do
        robot.direction = :east
        expect{ robot.move }.to change{ robot.x }.from(1).to(2)
      end

      it 'to north' do
        robot.direction = :north      
        expect{ robot.move }.to change{ robot.y }.from(1).to(2)
      end

      it 'to south' do
        robot.direction = :south
        expect{ robot.move }.to change{ robot.y }.from(1).to(0)
      end
    end

    context 'respect table limits' do
      context 'on X move' do
        it 'does not break Y limits moving west' do
          robot = described_class.new(0, 0, :west, 5, 5)

          expect{ robot.move }.not_to change{ robot.x }
        end

        it 'does not break Y limits moving east' do
          robot = described_class.new(4, 0, :east, 5, 5)
          
          expect{ robot.move }.not_to change{ robot.x }
        end
      end

      context 'on Y move' do
        it 'does not break Y limits moving south' do
          robot = described_class.new(0, 0, :south, 5, 5)

          expect{ robot.move }.not_to change{ robot.y }
        end

        it 'does not break Y limits moving north' do
          robot = described_class.new(4, 4, :north, 5, 5)
          
          expect{ robot.move }.not_to change{ robot.y }
        end
      end
    end
  end
end