
require_relative 'robot.rb'

class RobotGame

  # Table Dimensions are 5x5
  TABLE_LIMIT_X = 5
  TABLE_LIMIT_Y = 5

  def start
    # Ask for instructions     
    file_name = get_instructions_file
    # Read all instructions for the Robot from a file
    File.open("tests/#{file_name}.txt", "r").readlines.each do |line|
      @instruction = line.chomp.downcase
      if !robot_is_there && @instruction.start_with?("place ")
        x, y, direction = placement_params
        @robot = Robot.new(x, y, direction, TABLE_LIMIT_X, TABLE_LIMIT_Y)
      elsif robot_is_there && @robot.respond_to?(@instruction)
        @robot.send("#{@instruction}")
      else
        raise "Unknown/unvalid instruction: #{@instruction}"
      end
    end
    puts @robot.report
  end

  private

  def get_instructions_file
    puts "Enter the file to test (1, 2, 3, 4, 5, 6 or 7):"
    file_name = gets.chomp
  end

  # extract the placement params to initialize the robot
  # input format is 'PLACE x,y,direction'
  def placement_params
    params = @instruction.split(" ").last
    params = params.split(",")
    x = params.shift.to_i
    y = params.shift.to_i
    direction = params.shift
    if x.nil? || y.nil? || direction.nil?
      raise 'Please provide x, y and direction in the PLACE instruction'
    else
      return x, y, direction
    end
  end

  # A robot needs to be on the table before any other operation can be applied
  def robot_is_there
    @robot.nil? ? false : true
  end
end

game = RobotGame.new
game.start